let productList = [];
let cartProducts = [];
let listFilter = [];
let BASE_URL = "https://62f8b7553eab3503d1da15e2.mockapi.io";

let renderProductService = function () {
  axios({
    url: `${BASE_URL}/Product`,
    method: "GET",
  })
    .then(function (res) {
      productList = res.data;
      rendenSanPham(productList);
    })
    .catch(function (err) {});
};

renderProductService();
let rendenSanPham = function (list) {
  let contentHtml = "";
  list.forEach(function (item) {
    var cardItem = `
    <div class="card">
          <div class="top-bar">\n <em class="stocks">In Stock</em></div>
          <div class="img-container">
            <img class="product-img" src="${item.img}" alt="" />
            <div class="out-of-stock-cover"><span>Out Of Stock</span></div>
          </div>
          <div class="details">
            <div class="name-fav">
              <strong class="product-name">${item.name}</strong>
              <button onclick='this.classList.toggle("fav")' class="heart">
                <i class="fas fa-heart"></i></button
              >
            </div>
            <div class="wrapper">
              <p>BackCamera : ${item.backCamera}</p>
              <p>FrontCamera : ${item.frontCamera}</p>
              <p>${item.desc}</p>
              <p>${item.type}</p>
            </div>
            <div class="purchase">
              <p class="product-price">$ ${item.price}</p>
              <span class="btn-add"> 
                <div>  
                  <button onclick="addToCart('${item.id}')" class="add-btn">Add</button>
                </div>
              </span>
            </div>
          </div>
        </div>`;
    contentHtml += cardItem;
  });
  document.querySelector(".main-cart").innerHTML = contentHtml;
};

function displayGioHang() {
  let cart = document.querySelector(".cart");
  let t = document.querySelector(".side-nav");
  cart.style.display = "block";
  t.style.right = "0";
}

function hideGioHang() {
  let cart = document.querySelector(".cart");
  cart.style.display = "none";
}

let findIndexProduct = (arr, id) => {
  let index = arr.findIndex((vitri) => vitri.id == id);
  return index;
};

let findIndexCart = (arr, id) => {
  let index = arr.findIndex((vitri) => vitri.product.id == id);
  return index;
};

let renderGioHang = (list) => {
  let listCart = ``;
  list.forEach((item) => {
    let trContent = `
    <div class="cart-item">
        <div class="cart-img"><img src="${item.product.img}" alt="" /></div>
        <strong class="name">${item.product.name}</strong>\n
        <span class="qty-change">        
          <div>           
            <button class="btn-qty" onclick="descCartItem(${item.product.id})">
              <i class="fas fa-chevron-left"></i></button>
            <p class="qty">${item.quantity}</p>           
            <button class="btn-qty" onclick="incCartItem(${item.product.id})">
              <i class="fas fa-chevron-right"></i></button>
          </div>
        </span>
        <p class="price">$ ${item.quantity * item.product.price}</p>
        <button onclick="removeCartItem(${item.product.id})">
          <i class="fas fa-trash"></i></button>
      </div>`;
    listCart += trContent;
  });
  saveLocalStorage();
  return listCart;
};

let renderCartService = function (arr) {
  let renderCartList = renderGioHang(arr);
  document.querySelector(".cart-items").innerHTML = renderCartList;
};

let addToCart = (id) => {
  let indexProduct = findIndexProduct(productList, id);

  let indexCart = findIndexCart(cartProducts, id);
  if (indexCart >= 0) {
    cartProducts[indexCart].quantity++;
  } else {
    let productCart = new cartItem(productList[indexProduct]);
    cartProducts.push(productCart);
  }

  renderCartService(cartProducts);
  countProduct(cartProducts);
  totalCost(cartProducts);
};

let removeCartItem = (id) => {
  let index = findIndexCart(cartProducts, id);

  cartProducts.splice(index, 1);

  renderCartService(cartProducts);
  countProduct(cartProducts);
  totalCost(cartProducts);
};

let descCartItem = (id) => {
  let index = findIndexCart(cartProducts, id);

  if (cartProducts[index].quantity > 1) {
    cartProducts[index].quantity--;
  } else {
    removeCartItem(id);
  }

  renderCartService(cartProducts);
  countProduct(cartProducts);
  totalCost(cartProducts);
};

let incCartItem = (id) => {
  let index = findIndexCart(cartProducts, id);

  cartProducts[index].quantity++;

  renderCartService(cartProducts);
  countProduct(cartProducts);
  totalCost(cartProducts);
};

let totalCost = (arr) => {
  let total = 0;
  arr.forEach((item) => {
    let allItem = item.quantity * item.product.price;
    total += allItem;
  });
  document.querySelector(".total").innerHTML = total;
  document.querySelector(".pay").innerHTML = `$ ${total}`;
};

let clearCart = () => {
  cartProducts = [];
  renderCartService(cartProducts);
  countProduct(cartProducts);
  totalCost(cartProducts);
};

let confirmOrder = () => {
  let contentHtml = `
    <div>
        <div class="order-details">
            <em class="thanks">Thanks for shopping with us</em>
        </div>
        <button onclick="okay()" class="btn-ok">Continue</button>
    </div>
  `;
  document.querySelector(".invoice").style.height = "180px";
  document.querySelector(".invoice").innerHTML = contentHtml;

  renderCartService(cartProducts);
  clearCart();
};

let countProduct = (arr) => {
  let count = 0;
  arr.forEach((item) => {
    count += item.quantity;
  });
  document.querySelector(".total-qty").innerHTML = count;
};

let filterProduct = (products) => {
  let type = document.querySelector("#typeProduct").value;
  let listProduct = [];
  if (type == 0) {
    listProduct = products;
  } else {
    products.forEach((product) => {
      if (type.toUpperCase() === product.type.toUpperCase()) {
        listProduct.push(product);
      }
    });
  }
  return listProduct;
};

let renderProductFormType = () => {
  let list = filterProduct(productList);
  rendenSanPham(list);
};

function okay() {
  document.querySelector(".invoice").style.display = "none";
}

function displayInvoice(flag) {
  if (flag === true) {
    document.querySelector(".invoice").style.display = "block";
    document.querySelector(".side-nav").style.width = "0";
  } else {
    document.querySelector(".invoice").style.display = "none";
    document.querySelector(".side-nav").style.width = "60%";
  }
}

function displayGioHang(flag) {
  if (flag === true) {
    document.querySelector(".side-nav").style.width = "60%";
  } else {
    document.querySelector(".side-nav").style.width = "0";
  }
}

let saveLocalStorage = () => {
  let cartItems = JSON.stringify(cartProducts);
  localStorage.setItem("cartList", cartItems);
};

let getLocalStorage = () => {
  let cartItemLocal = localStorage.getItem("cartList");
  if (JSON.parse(cartItemLocal)) {
    cartProducts = JSON.parse(cartItemLocal);

    renderCartService(cartProducts);
    countProduct(cartProducts);
    totalCost(cartProducts);
  }
};
getLocalStorage();
